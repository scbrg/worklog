PREFIX = /usr/local

.PHONY: install
install: $(PREFIX)/bin/log $(PREFIX)/bin/worked

.PHONY: uninstall
uninstall:
	$(RM) $(PREFIX)/bin/log
	$(RM) $(PREFIX)/bin/worked

.PHONY: test
test:
	checkbashisms log
	shellcheck log

README.html: README.md
	markdown $< > $@

$(PREFIX)/bin/log: log
	install -m 755 $< $@

$(PREFIX)/bin/worked: worklog/worklog.py
	install -m 755 $< $@
