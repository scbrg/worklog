#!/usr/bin/env python3
import datetime
import functools
import sys

BEGINNING_OF_TIME = datetime.date(datetime.MINYEAR, 1, 1)


def time_diff(t1, t2):
    return datetime.timedelta(seconds=(
        (t1.hour - t2.hour) * 3600 +
        (t1.minute - t2.minute) * 60 +
        (t1.second - t2.second)
    ))


@functools.total_ordering
class Day:

    def __init__(self, date=BEGINNING_OF_TIME):
        self.date = date
        self.events = {}
        self.indent = '  '

    @property
    def arrived(self):
        left_home = False
        arrived_work = None
        for time, message in self:
            if message == 'leaving home':
                left_home = True
            if left_home and message.startswith('returned after'):
                arrived_work = time
                left_home = False
            if message == 'back at work':
                arrived_work = time
        else:
            if arrived_work is not None:
                return arrived_work

        for time, message in self:
            if message.startswith('returned after') and time.hour in range(8, 12):
                return time

    @property
    def left(self):
        for time, message in self:
            if message == 'home':
                return time

    @property
    def pauses(self):
        pause = None
        for time, message in self:
            if pause is None:
                if message.startswith('lunch'):
                    start = time
                    pause = message
            else:
                if message.startswith('returned after'):
                    end = time
                    diff = time_diff(end, start)
                    yield start, end, diff, pause
                    pause = None

    @property
    def lunch_start(self):
        for time, message in self:
            if message == 'lunch':
                return time

    @property
    def lunch_end(self):
        lunch = False
        for time, message in self:
            if message == 'lunch':
                lunch = True
            elif lunch and message.startswith('returned after'):
                return time

    @property
    def worked(self):
        away = sum((diff for _, _, diff, _ in self.pauses), datetime.timedelta())
        try:
            return time_diff(self.left, self.arrived) - away
        except Exception:
            return datetime.timedelta()

    def report(self, out):
        if self.date == BEGINNING_OF_TIME:
            return
        # if self.date.weekday() in [5, 6]:  # skip sat, sun
        #     return
        out.write(str(self.date))
        out.write('\n')
        out.write('  arrived:   {day.arrived}\n'.format(day=self))
        for start, end, diff, message in self.pauses:
            out.write('  {message:10} {start} - {end} ({diff})\n'.format(**locals()))
        out.write('  left:      {day.left}\n'.format(day=self))
        out.write('  worked:     {day.worked}'.format(day=self))

    def __iter__(self):
        return iter(sorted(self.events.items()))

    def __setitem__(self, key, value):
        self.events[key] = value.strip()

    def __eq__(self, other):
        return cmp(self, other) == 0

    def __ne__(self, other):
        return not self == other

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            other = other.date
        return self.date < other

    def __str__(self):
        return str(self.date)


@functools.total_ordering
class Month:

    def __init__(self, date=BEGINNING_OF_TIME):
        self.date = date.replace(day=1)
        self.days = []

    @property
    def working_days(self):
        for day in self.days:
            if day.arrived and day.left:
                yield day

    @property
    def worked(self):
        return sum((day.worked for day in self.working_days), datetime.timedelta())

    @property
    def worked_hours(self):
        return float(self.worked.total_seconds() / 3600)

    def append(self, day):
        self.days.append(day)

    def report(self, out):
        if self.date == BEGINNING_OF_TIME:
            return

        worked = datetime.timedelta()
        for day in self.days:
            day.report(out=out)
            worked += day.worked
            out.write('  (This month: {:3d}:{:02d}:{:02d})\n'.format(
                (worked.days * 86400 + worked.seconds) // 3600,
                worked.seconds % 3600 // 60,
                worked.seconds % 60
            ))

        out.write('Total working time in {self}: {self.worked_hours:.1f}\n'.
                  format(**locals()))

    def __eq__(self, other):
        return self.date == other.date

    def __ne__(self, other):
        return not self == other

    def __lt__(self, other):
        return self.date < other.date

    def __str__(self):
        return '{0.year}-{0.month}'.format(self.date)


def parse_line(line):
    dt = datetime.datetime.strptime(line[:19], '%Y-%m-%d %H:%M:%S')
    return dt, line[26:]


def report_day(day, out=sys.stdout):
    out.write(str(day['date']))


def parse_stream(stream, out):
    day = Day(datetime.date(datetime.MINYEAR, 1, 1))
    month = Month(day.date)
    for line in stream:
        ts, message = parse_line(line)
        if Month(ts.date()) > month:
            month.report(out)
            month = Month(ts.date())

        if ts.date() > day:
            day = Day(ts.date())
            month.append(day)

        day[ts.time()] = message

    month.report(out)

def parse_file(filename, out):
    with open(filename) as f:
        parse_stream(f, out)


def main(argv=sys.argv, stdin=sys.stdin, stdout=sys.stdout):
    if argv[1:]:
        for filename in argv[1:]:
            parse_file(filename, out=stdout)
    else:
        parse_stream(stdin, stdout)


if __name__ == '__main__':
    main()
