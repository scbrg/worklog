# Worklog

Simple tool for logging activity.

## Dependencies

- [Python 3](https://www.python.org/)
- [slock](https://tools.suckless.org/slock/)
- systemd (for `-p` and `-s` flags to `log`)
- [GNU Time](https://www.gnu.org/software/time/)
